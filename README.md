# Purpose

TODO

# TODO

- Pull in the node info from stdin
 (for Deploy nodes to be easily used in CI envs)
- Init and save node info
- Design the CLI
- 


# Codebase structure

Prekube is implemented as a single binary executable
that contains two major functionalities: client and server.
`main.rs` is the entry point of the executable, and
`client.rs` and `server.rs` implement the two functionalities.

A major shared features between the two are:
 - Message passing between the client and the server
  (message schemas, encoding and decoding)
 part implemented in `messages.rs`.
 - Saving the state of the cluster and the node,
 implemented in `data_store.rs`.
 The state is typically stored in `/var/lib/prekube/state.json`.
 - The most important piece of "shared vocabulary" here is `Task`,
implemented in `task.rs`.
A task refers to a single,
running "service" that can be deployed on the cluster.
It is backed up by a Docker registry,
as a combination of an URL and a tag.

# How it works

There is:

- A cluster, that consists of
- Nodes and
- Tasks

Each node as an UUID that identifies it,
and a public and private keys that are used to authenticate it's messages.

A node can be a server but it doesn't need to be.
Nodes communicate with each others.

A simple scenario:

- Server node (on server, with IP 123.123.123.123)
- Master node (on dev computer)
- Deployer node (in CI system)

Flow:
- Install Prekube to Server and Master computers
- `M$ prekube cluster init "ganbare"`
- The command generates a pre-shared key and a master node
and saves them into a file `ganbare-master.json` with permissions 0500,
warning that it contains secrets.
It also creates `/var/lib/prekube/ganbare-node.json`.
  - `ganbare-master.json` contains the PSK, master UUID and master public and private keys.
  - `ganbare-node.json` contains the PSK, node UUID and public and private keys
  and master UUID & public key.
- The command outputs a copy-pasteable (base64?) value
  - The value contains the PSK, master UUID and the public key
- `S$ prekube cluster join "ganbare"`
- The command asks to paste in the value
- It then creates `/var/lib/prekube/ganbare-node.json` that contains the PSK,
node UUID,
node public and private keys
- `S$ prekube server` starts Prekube as a daemonized process that
manages Docker and responds to queries to it's port
- `M$ prekube connect "123.123.123.123"`
  - This updates/creates `/var/lib/prekube/ganbare-cluster.json` on Master
  that contains IP addresses, UUIDs and public keys of cluster members
- `M$ prekube cluster new-node`