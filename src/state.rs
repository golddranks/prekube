use std::fs;
use std::path::Path;

use hex::{FromHex, ToHex};
use serde::{Serializer, Deserialize, Deserializer};
use uuid::Uuid;

use crate::Error;
use crate::Task;
use crate::config;

const LOAD_ERROR: &str = "loading state file";

#[derive(Serialize, Deserialize)]
pub struct State {
    version: u64,
    tasks: Vec<Task>,
}

#[derive(Serialize, Deserialize)]
pub struct Node {
    id: Uuid,
    private_key: Vec<u8>,
    public_key: Vec<u8>,
}

pub fn load_from_file(path: impl AsRef<Path>) -> Result<State, Error> {
    let state_str = fs::read_to_string(path).map_err(|e| Error::from(e, LOAD_ERROR))?;

    let state: State = serde_json::from_str(&state_str).map_err(|e| Error::from(e, "parsing state file JSON"))?;

    Ok(state)
}

pub fn load_or_create_default() -> Result<State, Error> {
    match load_from_file(config::STATE_PATH) {
        Err(e) if e.context == LOAD_ERROR => Ok(
            State {
                tasks: vec![],
            }
        ),
        result => result,
    }
}

pub fn buf_to_hex<T, S>(buf: &T, serializer: S) -> Result<S::Ok, S::Error>
    where T: AsRef<[u8]>,
          S: Serializer
{
    serializer.serialize_str(&buf.as_ref().to_hex())
}

pub fn hex_to_buf<'de, D>(deserializer: D) -> Result<Vec<u8>, D::Error>
    where D: Deserializer<'de>
{
    use serde::de::Error;
    String::deserialize(deserializer)
        .and_then(|string| Vec::from_hex(&string).map_err(|err| Error::custom(err.to_string())))
}