use log::{info, warn};
use std::net::ToSocketAddrs;
use tunnel::TunnelError;

use crate::docker::{self, Docker};
use crate::Message;
use crate::config;
use crate::state;

const PSK: &[u8] = b"1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A";

pub fn run() {
    let state = state::load_or_create_default().expect("TODO");
    let listener = match std::net::TcpListener::bind((config::DEFAULT_BIND_IP, config::DEFAULT_PORT)) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Couldn't bind the address! {}", e);
            std::process::exit(4);
        }
    };

    info!("Listening on {}:{}", config::DEFAULT_BIND_IP, config::DEFAULT_PORT);
    for stream_res in listener.incoming() {
        let stream = match stream_res {
            Ok(s) => s,
            Err(e) => {
                eprintln!("Error while establishing an incoming connection! {}", e);
                continue;
            }
        };
        let mut stream = match tunnel::server(stream, tunnel::SimplePskProvider::new(PSK)) {
            Ok(s) => s,
            Err(e) => {
                eprintln!("Error while initialising the connection! {}", e);
                continue;
            }
        };

        {
            use Message::*;

            match Message::parse(stream).expect("TODO") {
                Deploy(d) => {
                    let d = docker::new().expect("TODO");
                    d.deploy()
                },
            }
        }
    }
}
