use data_encoding::HEXUPPER;
use log::info;
use std::io::{Read, Write};
use std::net::TcpStream;
use std::sync::Arc;

const PSK: &[u8] = b"1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A1A";

pub fn run() {
    let mut stream = tunnel::connect_simple("localhost:10443", "test_id", PSK).unwrap();

    let mut res = vec![];
    stream.read_to_end(&mut res).unwrap();
    println!("{}", String::from_utf8_lossy(&res));
}
