use bollard::{Docker as DockerApi, image::CreateImageOptions, container::{CreateContainerOptions, Config}};
use tokio::runtime::Runtime;
use hyper::client::connect::Connect;
use futures::{future::Future, stream::Stream};

use crate::Error;
use crate::Task;
use bollard::DockerChain;

pub struct Docker<C> {
    api: DockerApi<C>,
    rt: Runtime,
}

    pub fn new() -> Result<Docker<impl Connect>, Error> {
        Ok(Docker {
            api: DockerApi::connect_with_unix_defaults().map_err(|e| Error::from(e.compat(), "connecting to Docker socket"))?,
            rt: Runtime::new().map_err(|e| Error::from(e, "initialising Tokio runtime"))?,
        })
    }

impl<D> Docker<D> where D: Connect + 'static {

    pub fn deploy(&mut self, task: &Task) -> Result<(), Error> {
        let credentials = None; // TODO
        let api2 = self.api.clone();
        let runtime = &mut self.rt;
        let fut = self.api
            .create_image(Some(CreateImageOptions::<String> {
                from_src: task.pull_url(),
                ..Default::default()
            }), credentials)
            .into_future()
            .map_err(|(e, _stream)| Error::from(e.compat(), "create image"))
            .and_then(move|_|
                api2.create_container(
                None::<CreateContainerOptions<String>>,
                Config {
                    image: Some("hello-world"),
                    cmd: Some(vec!["/hello"]),
                    ..Default::default()
                }).map_err(|e| Error::from(e.compat(), "create container")));
        runtime.block_on(fut);
        Ok(())
    }

    pub fn shutdown(self) {
        let Docker { rt, api } = self;
        rt.shutdown_now().wait().expect("Error on shutdown!");
    }
}
