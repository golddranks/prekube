use std::io::Read;
use serde::{Serialize, Deserialize};

use crate::Error;
use crate::Task;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Deploy {
    task_id: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UpdateCluster {
    state:
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Message {
    Deploy(Deploy),
    UpdateCluster(Addtask),
}

impl Message {
    pub fn parse(input: impl Read) -> Result<Message, Error> {
        let msg: Message = serde_cbor::from_reader(input).map_err(|e| Error::from(e, "parsing the incoming message"))?;

        Ok(msg)
    }
}