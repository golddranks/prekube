use std::collections::HashMap;

use uuid::Uuid;

use crate::error::Error;

#[derive(Serialize, Deserialize)]
pub struct Task {
    id: Uuid,
    name: String,
    repository: String,
    tag: String,
}

impl Task {
    pub fn pull_url(&self) -> String {
        format!("{}:{}", self.repository, self.tag)
    }
}

pub fn load() -> HashMap<String, Task> {
    let mut hash_map = HashMap::new();

    hash_map.insert(Task {
        name: "test".to_owned(),
        repository: ""
    });

    hash_map
}