use std::error::Error as StdError;
use std::fmt;

#[derive(Debug)]
pub struct Error {
    pub context: &'static str,
    pub inner: Option<Box<dyn StdError + Send + 'static>>
}

impl Error {
    pub fn from(e: impl StdError + Send + 'static, context: &'static str) -> Error {
        Self {
            context,
            inner: Some(Box::new(e)),
        }
    }

    pub fn new(context: &'static str) -> Error {
        Self {
            context,
            inner: None,
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        self.inner.as_ref().map(AsRef::as_ref).map(|obj| obj as &(dyn StdError + 'static))
    }
}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match &self.inner {
            Some(e) => write!(fmt, "{}: {}", self.context, e),
            None => write!(fmt, "{}", self.context),
        }
    }
}