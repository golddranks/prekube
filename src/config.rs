pub const STATE_PATH: &str = "/var/lib/prekube/state.json";
pub const DEFAULT_PORT: u16 = 10443;
pub const DEFAULT_BIND_IP: &str = "0.0.0.0";