use log::{info, warn};

mod client;
mod server;
mod init;
mod docker;
mod error;
mod task;
mod messages;
mod config;
mod state;

pub use docker::Docker;
pub use error::Error;
pub use task::Task;
pub use messages::Message;

#[derive(StructOpt, Debug)]
enum Opt {
    Server,
    Deploy{ task_name: String },
    AddTask{ name: String, docker_url: String },
    Init,
}

fn main() {
    env_logger::init();



    match Opt::from_args() {
        Opt::AddTask { name, docker_url } => {

        },
        Opt::Server => {

        },
        Opt::Init => {
            init::run();
        },
        Opt::Deploy { task_name} => {

        }
    }
}
